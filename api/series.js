const https = require("https");

const MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000;

function differenceDays(date0, date1) {
    return Math.floor(
        (date1.valueOf() - date0.valueOf()) / MILLISECONDS_PER_DAY
    );
}

module.exports = async function (req, res) {
    const now = new Date();
    const params = Object.assign(
        {
            "dataPeriod": "Day",
            "days": 2,
            "startDate": now.toISOString(),
            "symbol": "",
        },
        req.query
    );

    params.days = Number(params.days);
    params.endOffsetDays = differenceDays(new Date(params.startDate), now) -
        params.days + 1;
    params.days += 1;

    if (params.endOffsetDays <= 0) {
        params.days += params.endOffsetDays;
        params.endOffsetDays = 0;
    }

    const response = await new Promise(function (resolve) {
        const query = https.request(
            "https://markets.ft.com/data/chartapi/series",
            resolve,
        );
        forwardHeaders = ["accept", "accept-encoding"];
        forwardHeaders.forEach(function (header) {
            if (req.headers[header]) {
                query.setHeader(header, req.headers[header]);
            }
        });
        const request = JSON.stringify({
            "dataInterval": 1,
            "dataNormalized": false,
            "dataPeriod": params.dataPeriod,
            "days": params.days,
            "elements": [
                {
                    "OverlayIndicators": [],
                    "Params": {},
                    "Symbol": params.symbol,
                    "Type": "price",
                },
            ],
            "endOffsetDays": params.endOffsetDays,
            "realtime": false,
            "returnDateType": "ISO8601",
            "yFormat": "0.###",
        });
        query.setHeader("Content-Type", "application/json");
        query.setHeader("Content-Length", request.length);
        query.end(request);
    });

    returnHeaders = [
        "content-encoding",
        "content-length",
        "content-type",
    ];
    returnHeaders.forEach(function (header) {
        if (response.headers.hasOwnProperty(header)) {
            res.setHeader(header, response.headers[header]);
        }
    });
    response.pipe(res);
};
