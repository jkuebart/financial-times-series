financial-times-series
======================

This serverless function provides a `GET` endpoint for Financial Times'
JSON API at https://markets.ft.com/data/chartapi/series.

The `api/series.js` endpoint has three parameters:
  - `symbol`: The desired symbol (required).
  - `startDate`: The first date of the series, default today.
  - `days`: The number of days, default 2.

The API is provided at https://financial-times-series.vercel.app/api/series.js.

This can be used as a data source for historical prices in
[Portfolio Performance][PP]. The URL would be

    https://financial-times-series.vercel.app/api/series.js?days=366&startDate={DATE:yyyy-01-01}&symbol={TICKER}

and the JSON expressions for date and price, respectively:

    $.Dates[*]
    $.Elements[0].ComponentSeries[?(@.Type == "Close")].Values[*]


Testing
-------

There is a test in `test/series.js` which invokes the API for each of the
symbols given on the command line and prints the response.


Licence
-------

`financial-times-series` is free software licenced under the [3-clause BSD
licence][BSD3] licence, see [LICENCE](LICENCE).


[BSD3]: https://opensource.org/licenses/BSD-3-Clause
[PP]: https://www.portfolio-performance.info/
