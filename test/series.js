#!/usr/bin/env node

const series = require("../api/series.js");

process.argv.slice(2).forEach(function (arg) {
    const response = Object.create(process.stdout);
    response.setHeader = function (header, value) {
        console.log("Header", header, value);
    }
    series(
        {
            "headers": {},
            "query": {
                "days": "10",
                "startDate": "2021-01-01",
                "symbol": arg,
            },
        },
        response
    );
});
